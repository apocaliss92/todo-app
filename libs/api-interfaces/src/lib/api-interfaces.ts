import { TodoEntity } from '../../../../apps/api/src/app/todos/todo.entity';
import { Version as VersionDto } from '../../../../apps/api/src/app/dto/version';
import { UpsertTodo as UpsertTodoDto } from '../../../../apps/api/src/app/todos/dto/upsert-todo';

export type Todo = TodoEntity;
export type UpsertTodo = UpsertTodoDto;
export type Version = VersionDto;
