import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { ApiDefaultResponse } from '@nestjs/swagger';
import { Version } from './dto/version';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {
  }

  @ApiDefaultResponse({
    description: 'Get application version.',
    type: String,
  })
  @Get('version')
  getVersion(): Version {
    return {
      version:  `${this.appService.getVersion()}`
    };
  }
}
